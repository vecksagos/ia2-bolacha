#include "person.h"
#include <iostream>
#include <utility>
#include <cmath>

using namespace std;

Person::Person(Age age, Income income, bool student
               , Credit_rating credit_rating)
    : age(age)
    , income(income)
    , student(student)
    , credit_rating(credit_rating) {

}

Person::Person(Age age, Income income, bool student, Credit_rating credit_rating
               , bool computer)
    : age(age)
    , income(income)
    , student(student)
    , credit_rating(credit_rating)
    , computer(computer) {

}

uint8_t Person::chooser(vector<Person> persons
                                  , vector<Indepedents_terms> terms) {
    double entropyS = entropy(persons, Indepedents_terms::COMPUTER);
    if ((entropyS == 0) || (entropyS == 1))
        return (uint8_t) Indepedents_terms::COMPUTER;

    pair<double, Indepedents_terms> par = make_pair(0, (Indepedents_terms) 0x0);

    uint8_t look = 0x0;
    for (auto term : terms)
        look |= (uint8_t) term;


    if (look & 0x1) {
        double entropyV = entropy(persons, Indepedents_terms::INCOME);
        if (entropyS - entropyV > par.first) {
            par.first = entropyS - entropyV;
            par.second = Indepedents_terms::INCOME;
        }
    }

    if ((look >> 1) & 0x1) {
        double entropyV = entropy(persons, Indepedents_terms::STUDENT);
        if (entropyS - entropyV > par.first) {
            par.first = entropyS - entropyV;
            par.second = Indepedents_terms::STUDENT;
        }
    }

    if ((look >> 2) & 0x1) {
         double entropyV = entropy(persons, Indepedents_terms::CREDIT_RATING);
        if (entropyS - entropyV > par.first) {
            par.first = entropyS - entropyV;
            par.second = Indepedents_terms::CREDIT_RATING;
        }
    }

    if ((look >> 3) & 0x1) {
        double entropyV = entropy(persons, Indepedents_terms::AGE);
        if (entropyS - entropyV > par.first) {
            par.first = entropyS - entropyV;
            par.second = Indepedents_terms::AGE;
        }
    }

    return (uint8_t) par.second;
}

double Person::entropy(vector<Person> persons, Indepedents_terms term) {
    switch(term) {
    case Indepedents_terms::AGE: {
        pair<int, int> lE30pair = make_pair(0, 0);
        pair<int, int> bT3140pair = make_pair(0, 0);
        pair<int, int> g40pair = make_pair(0, 0);

        for (auto person : persons) {
            switch(person.age) {
            case Age::LE30: {
                if (person.computer)
                    ++lE30pair.first;
                ++lE30pair.second;
            } break;
            case Age::BT3140: {
                if (person.computer)
                    ++bT3140pair.first;
                ++bT3140pair.second;
            } break;
            case Age::G40: {
                if (person.computer)
                    ++g40pair.first;
                ++g40pair.second;
            } break;
            }
        }

        double size = persons.size()
            , leSize = lE30pair.second
            , btSize = bT3140pair.second
            , gSize = g40pair.second
            , lE30buy = lE30pair.first
            , bT3140buy = bT3140pair.first
            , g40buy = g40pair.first
            , leProp = lE30buy / leSize
            , btProp = bT3140buy / btSize
            , gProp = g40buy / gSize;

        double leEntropy = 0;
        if (!((leProp == 0) || (leProp == 1)))
            leEntropy = -(leProp * log2(leProp))
                - ((1 - leProp) * log2(1 - leProp));

        double btEntropy = 0;
        if (!((btProp == 0) || (btProp == 1)))
            btEntropy = -(btProp * log2(btProp))
                - ((1 - btProp) * log2(1 - btProp));

        double gEntropy = 0;
        if (!((gProp == 0) || (gProp == 1)))
            gEntropy = -(gProp * log2(gProp))
                - ((1 - gProp) * log2(1 - gProp));

        return ((leSize / size) * leEntropy) + ((btSize / size) * btEntropy)
            + ((gSize / size) * gEntropy);
    } break;

    case Indepedents_terms::INCOME: {
        pair<int, int> highPair = make_pair(0, 0)
            , mediumPair = make_pair(0, 0)
            , lowPair = make_pair(0, 0);

        for (auto person : persons) {
            switch(person.income) {
            case Income::HIGH: {
                if (person.computer)
                    ++highPair.first;
                ++highPair.second;
            } break;
            case Income::MEDIUM: {
                if (person.computer)
                    ++mediumPair.first;
                ++mediumPair.second;
            } break;
            case Income::LOW: {
                if (person.computer)
                    ++lowPair.first;
                ++lowPair.second;
            } break;
            }
        }

        double size = persons.size()
            , highSize = highPair.second
            , mediumSize = mediumPair.second
            , lowSize = lowPair.second
            , highBuy = highPair.first
            , mediumBuy = mediumPair.first
            , lowBuy = lowPair.first
            , highProp = highBuy / highSize
            , mediumProp = mediumBuy / mediumSize
            , lowProp = lowBuy / lowSize;

        double highEntropy = 0;
        if (!((highProp == 0) || (highProp == 1)))
            highEntropy = -(highProp * log2(highProp))
                - ((1 - highProp) * log2(1 - highProp));

        double mediumEntropy = 0;
        if (!((mediumProp == 0) || (mediumProp == 1)))
            mediumEntropy = -(mediumProp * log2(mediumProp))
                - ((1 - mediumProp) * log2(1 - mediumProp));

        double lowEntropy = 0;
        if (!((lowProp == 0) || (lowProp == 1)))
            lowEntropy = -(lowProp * log2(lowProp))
                - ((1 - lowProp) * log2(1 - lowProp));

        return ((highSize / size) * highEntropy)
            + ((mediumSize / size) * mediumEntropy)
            + ((lowSize / size) * lowEntropy);
    } break;

    case Indepedents_terms::STUDENT: {
        pair<int, int> truePair = make_pair(0, 0)
            , falsePair = make_pair(0, 0);

        for (auto person : persons)
            if (person.student) {
                if (person.computer)
                    ++truePair.first;
                ++truePair.second;
            } else {
                if (person.computer)
                    ++falsePair.first;
                ++falsePair.second;
            }

        double size = persons.size()
            , trueSize = truePair.second
            , falseSize = falsePair.second
            , trueBuy = truePair.first
            , falseBuy = falsePair.first
            , trueProp = trueBuy / trueSize
            , falseProp = falseBuy / falseSize;

        double trueEntropy = 0;
        if (!((trueProp == 0) || (trueProp == 1)))
            trueEntropy = -(trueProp * log2(trueProp))
                - ((1 - trueProp) * log2(1 - trueProp));

        double falseEntropy = 0;
        if (!((falseProp == 0) || (falseProp == 1)))
            falseEntropy = -(falseProp * log2(falseProp))
                - ((1 - falseProp) * log2(1 - falseProp));

        return ((trueSize / size) * trueEntropy)
            + ((falseSize / size) * falseEntropy);
    } break;

    case Indepedents_terms::CREDIT_RATING: {
        pair<int, int> fairPair = make_pair(0, 0)
            , excellentPair = make_pair(0, 0);

        for (auto person : persons)
            switch(person.credit_rating) {
            case Credit_rating::FAIR: {
                if (person.computer)
                    ++fairPair.first;
                ++fairPair.second;
            } break;
            case Credit_rating::EXCELLENT: {
                if (person.computer)
                    ++excellentPair.first;
                ++excellentPair.second;
            } break;
            }

        double size = persons.size()
            , fairSize = fairPair.second
            , excellentSize = excellentPair.second
            , fairBuy = fairPair.first
            , excellentBuy = excellentPair.first
            , fairProp = fairBuy / fairSize
            , excellentProp = excellentBuy / excellentSize;

        double excellentEntropy = 0;
        if (!((excellentProp == 0) || (excellentProp == 1)))
            excellentEntropy = -(excellentProp * log2(excellentProp))
                - ((1 - excellentProp) * log2(1 - excellentProp));

        double fairEntropy = 0;
        if (!((fairProp == 0) || (fairProp == 1)))
            fairEntropy = -(fairProp * log2(fairProp))
                - ((1 - fairProp) * log2(1 - fairProp));

        return ((fairSize / size) * fairEntropy)
            + ((excellentSize / size) * excellentEntropy);
    } break;

    case Indepedents_terms::COMPUTER: {
        int computer = 0;

        for (auto person : persons)
            if (person.computer)
                ++computer;

        double computerSize = computer
            , size = persons.size()
            , prop = computerSize / size;

        if ((prop == 1) || (prop == 0))
            return 1;
        else
            return -((prop * log2(prop)) + ((1 - prop) * log2(1 - prop)));
    } break;
    }
}
