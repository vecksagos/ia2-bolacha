#ifndef PERSON_H
#define PERSON_H

#include <vector>
#include <cstdint>


enum class Income { LOW, MEDIUM, HIGH };
enum class Indepedents_terms : uint8_t { INCOME = 0x1
        , STUDENT = 0x2
        , CREDIT_RATING = 0x4, AGE = 0x8 , COMPUTER = 0x10 };
enum class Credit_rating { FAIR, EXCELLENT };
enum class Age { LE30, BT3140, G40 };

class Person {
public:
    Person(Age age, Income income, bool student, Credit_rating credit_rating);
    Person(Age age, Income income, bool student
           , Credit_rating credit_rating, bool computer);

    static uint8_t chooser(std::vector<Person> persons
                                     , std::vector<Indepedents_terms> terms);
    static double entropy(std::vector<Person> persons, Indepedents_terms term);
private:
    Income income;
    bool student;
    Credit_rating credit_rating;
    bool computer;
    Age age;
};

#endif /* PERSON_H */
