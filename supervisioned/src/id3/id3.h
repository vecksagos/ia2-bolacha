#ifndef ID3_H
#define ID3_H

#include <vector>
#include "person.h"

class Id3 {
public:
    Id3();

private:
    std::vector<Person> db;
};

#endif /* ID3_H */
