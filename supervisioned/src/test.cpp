#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "id3/id3.h"
#include "id3/person.h"

using namespace std;

TEST_CASE("Initial Case ", "[id3]") {
    Person p1(Age::LE30, Income::HIGH, false, Credit_rating::FAIR, false);
    Person p2(Age::LE30, Income::HIGH, false, Credit_rating::EXCELLENT, false);
    Person p3(Age::BT3140, Income::HIGH, false, Credit_rating::FAIR, true);
    Person p4(Age::G40, Income::MEDIUM, false, Credit_rating::FAIR, true);
    Person p5(Age::G40, Income::LOW, true, Credit_rating::FAIR, true);
    Person p6(Age::G40, Income::LOW, true, Credit_rating::EXCELLENT, false);
    Person p7(Age::BT3140, Income::LOW, true, Credit_rating::EXCELLENT, true);
    Person p8(Age::LE30, Income::MEDIUM, false, Credit_rating::FAIR, false);
    Person p9(Age::LE30, Income::LOW, true, Credit_rating::FAIR, true);
    Person p10(Age::G40, Income::MEDIUM, true, Credit_rating::FAIR, true);
    Person p11(Age::LE30, Income::MEDIUM, true, Credit_rating::EXCELLENT, true);
    Person p12(Age::BT3140, Income::MEDIUM, false
               , Credit_rating::EXCELLENT, true);
    Person p13(Age::BT3140, Income::HIGH, true, Credit_rating::FAIR, true);
    Person p14(Age::G40, Income::MEDIUM, false
               , Credit_rating::EXCELLENT, false);

    vector<Person> v;
    v.push_back(p1);
    v.push_back(p2);
    v.push_back(p3);
    v.push_back(p4);
    v.push_back(p5);
    v.push_back(p6);
    v.push_back(p7);
    v.push_back(p8);
    v.push_back(p9);
    v.push_back(p10);
    v.push_back(p11);
    v.push_back(p12);
    v.push_back(p13);
    v.push_back(p14);

    REQUIRE(Person::entropy(v, Indepedents_terms::COMPUTER)
            == Approx(0.94028).epsilon(0.01));
    REQUIRE(Person::entropy(v, Indepedents_terms::AGE)
            == Approx(0.69353).epsilon(0.01));
    REQUIRE(Person::entropy(v, Indepedents_terms::INCOME)
            == Approx(0.91106).epsilon(0.01));
    REQUIRE(Person::entropy(v, Indepedents_terms::STUDENT)
            == Approx(0.78845).epsilon(0.01));
    REQUIRE(Person::entropy(v, Indepedents_terms::CREDIT_RATING)
            == Approx(0.89215).epsilon(0.01));

    vector<Indepedents_terms> terms;
    terms.push_back(Indepedents_terms::AGE);
    terms.push_back(Indepedents_terms::INCOME);
    terms.push_back(Indepedents_terms::CREDIT_RATING);
    terms.push_back(Indepedents_terms::STUDENT);

    REQUIRE(Person::chooser(v, terms) == (uint8_t) Indepedents_terms::AGE);
}

TEST_CASE("Left Case", "[id3]") {
    Person p1(Age::LE30, Income::HIGH, false, Credit_rating::FAIR, false);
    Person p2(Age::LE30, Income::HIGH, false, Credit_rating::EXCELLENT, false);
    Person p3(Age::LE30, Income::MEDIUM, false, Credit_rating::FAIR, false);
    Person p4(Age::LE30, Income::LOW, true, Credit_rating::FAIR, true);
    Person p5(Age::LE30, Income::MEDIUM, true, Credit_rating::EXCELLENT, true);

    vector<Person> v;
    v.push_back(p1);
    v.push_back(p2);
    v.push_back(p3);
    v.push_back(p4);
    v.push_back(p5);

    vector<Indepedents_terms> terms;
    terms.push_back(Indepedents_terms::INCOME);
    terms.push_back(Indepedents_terms::CREDIT_RATING);
    terms.push_back(Indepedents_terms::STUDENT);

    REQUIRE(Person::chooser(v, terms) == (uint8_t) Indepedents_terms::STUDENT);
}

TEST_CASE("Middle Case", "[id3]") {
    Person p1(Age::BT3140, Income::HIGH, false, Credit_rating::FAIR, true);
    Person p2(Age::BT3140, Income::LOW, true, Credit_rating::EXCELLENT, true);
    Person p3(Age::BT3140, Income::MEDIUM, false, Credit_rating::EXCELLENT
              , true);
    Person p4(Age::BT3140, Income::HIGH, true, Credit_rating::FAIR, true);

    vector<Person> v;
    v.push_back(p1);
    v.push_back(p2);
    v.push_back(p3);
    v.push_back(p4);

    vector<Indepedents_terms> terms;
    terms.push_back(Indepedents_terms::INCOME);
    terms.push_back(Indepedents_terms::CREDIT_RATING);
    terms.push_back(Indepedents_terms::STUDENT);

    REQUIRE(Person::chooser(v, terms) == (uint8_t) Indepedents_terms::COMPUTER);
}

TEST_CASE("Right Case", "[id3]") {
    Person p1(Age::G40, Income::MEDIUM, false, Credit_rating::FAIR, true);
    Person p2(Age::G40, Income::LOW, true, Credit_rating::FAIR, true);
    Person p3(Age::G40, Income::LOW, true, Credit_rating::EXCELLENT, false);
    Person p4(Age::G40, Income::MEDIUM, true, Credit_rating::FAIR, true);
    Person p5(Age::G40, Income::MEDIUM, false, Credit_rating::EXCELLENT, false);

    vector<Person> v;
    v.push_back(p1);
    v.push_back(p2);
    v.push_back(p3);
    v.push_back(p4);
    v.push_back(p5);

    vector<Indepedents_terms> terms;
    terms.push_back(Indepedents_terms::INCOME);
    terms.push_back(Indepedents_terms::CREDIT_RATING);
    terms.push_back(Indepedents_terms::STUDENT);

    REQUIRE(Person::chooser(v, terms)
            == (uint8_t) Indepedents_terms::CREDIT_RATING);
}
