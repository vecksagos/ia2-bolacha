#ifndef NODE_H
#define NODE_H

#include "state/state.h"
#include <functional>

class Node {
public:
    const Node *father;
    State state;
    Node(State state, const Node *father);

    bool operator==(const Node &rhs) const;

    bool isSolution(State solution);
    void print() const;
};

namespace std {
template<>
struct hash<Node>
{
    size_t operator ()(const Node &) const {
        return 42;
    }
};
}

#endif /* NODE_H */
