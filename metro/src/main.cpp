#include "graph/graph.h"
#include "state/state.h"
#include "node/node.h"
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;
using boost::lexical_cast;
using boost::bad_lexical_cast;

enum returns { OK, ERROR };

int main(int argc, char **argv) {
    if (argc != 3)
        return ERROR;

    int input1, input2;
    try {
        input1 = lexical_cast<int>(argv[1]);
        input2 = lexical_cast<int>(argv[2]);
    } catch(bad_lexical_cast &e) {
        cout << e.what() << endl;
    }

    if (!((0 <= input1 && input1 <= 13) || (0 <= input2 && input2 <= 13)))
        return ERROR;

    Graph graph(input2);
    graph.add(0, 1);
    graph.add(1, 2);
    graph.add(1, 8);
    graph.add(1, 9);
    graph.add(2, 3);
    graph.add(2, 8);
    graph.add(2, 12);
    graph.add(3, 4);
    graph.add(3, 7);
    graph.add(3, 12);
    graph.add(4, 5);
    graph.add(4, 6);
    graph.add(4, 7);
    graph.add(7, 8);
    graph.add(7, 11);
    graph.add(8, 10);
    graph.add(12, 13);

    State initial(input1);
    State final(input2);

    Node initialnode(initial, NULL);
    vector<Node> frontier;
    frontier.push_back(initialnode);

    unordered_set<Node> visited;
    auto current = frontier.back();
    while(!frontier.empty()) {
        for(auto it = frontier.begin(); it !=frontier.end(); ++it) {
            if (*it == current) {
                frontier.erase(it);
                break;
            }
        }
        visited.insert(current);

        if (current.state == final) {
            cout << "Answer:" << endl;
            current.print();
            cout << "Cost: " << current.state.cost;
            break;
        }

        for (int i = 0; i < MAXVERTEXES; ++i) {
            if ((!graph.adj(current.state.vertex, i)
                 || (current.state.vertex == i)))
                continue;

            auto son = graph.birth(current.state.vertex, i, current.state.cost);
            auto father = &*visited.find(current);
            Node gerated(son, father);

            frontier.push_back(gerated);
        }

        current = frontier.back();
        for (auto node : frontier) {
            if ((node.state.cost + graph.heuristic[node.state.vertex][input2])
                < (current.state.cost
                   + graph.heuristic[current.state.vertex][input2]))
                current = node;
        }
    }
    return 0;
}
