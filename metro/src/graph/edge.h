#ifndef EDGE_H
#define EDGE_H

#include <string>

class Edge {
    int cost;
    int heuristic;
    std::string name;

public:
    Edge(std::string name, int heuristic = 0, int cost = 0);
    void increasecost(int cost);
    int movecost(int cost);
};

#endif /* EDGE_H */
