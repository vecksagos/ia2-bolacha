#include "graph.h"
#include <utility>
#include <stack>
#include <iostream>

using namespace std;

Graph::Graph(int final)
    : final(final) {
    for (int i = 0; i < MAXVERTEXES; ++i) {
        vertexes[i].visited = false;
         for (int j = 0; j < MAXVERTEXES; ++j)
            arcs[i][j].adj = false;
    }
}


void Graph::add(int vertex1, int vertex2) {
    arcs[vertex2][vertex1].adj = true;
    arcs[vertex1][vertex2].adj = true;
}


bool Graph::adj(int vertex1, int vertex2) {
    return arcs[vertex1][vertex2].adj;
}

State Graph::birth(int vertex1, int vertex2, int cost) {
    int finalcost = cost + heuristic[vertex1][vertex2];

    State father(vertex1);
    State son(vertex2);
    if (!father.sameline(son))
        finalcost += (0.07 * 30);
    State state(vertex2, finalcost);

    return state;
}

void Graph::print() {
    for (int i = 0; i < MAXVERTEXES; ++i) {
        for (int j = 0; j < MAXVERTEXES; ++j)
            cout << arcs[i][j].adj;
        cout << endl;
    }
}
