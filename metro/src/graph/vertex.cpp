#include "vertex.h"

using namespace std;

Vertex::Vertex(Edge &edge1, Edge &edge2, int cost)
    : edge1(edge1)
    , edge2(edge2)
    , cost(cost) {
}

int Vertex::move() {
    return edge1.movecost(cost);
}
