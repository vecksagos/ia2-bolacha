#ifndef VERTEX_H
#define VERTEX_H

#include <utility>
#include "edge.h"

class Vertex {
    Edge edge1;
    Edge edge2;
    int cost;

public:
    Vertex(Edge &edge1, Edge &edge2, int cost = 0);
    int move();
};

#endif /* VERTEX_H */
