#include "edge.h"

using namespace std;

Edge::Edge(string name, int heuristic, int cost)
    : cost(cost)
    , heuristic(heuristic)
    , name(name) {

}

void Edge::increasecost(int cost) {
    this->cost += cost;
}

int Edge::movecost(int cost) {
    return this->cost + cost;
}
