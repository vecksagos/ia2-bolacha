#include "state.h"
#include <iostream>

using namespace std;

State::State(int vertex, int cost)
    : cost(cost)
    , vertex(vertex) {
    if (vertex == 0 || vertex == 5)
        line = 0x4;
    else if (vertex == 9 || vertex == 6)
        line = 0x2;
    else if (vertex == 10)
        line = 0x8;
    else if (vertex == 11 || vertex == 13)
        line = 0x1;
    else if (vertex == 1 || vertex == 4)
        line = 0x1 | 0x4;
    else if (vertex == 2)
        line = 0x8 | 0x4;
    else if (vertex == 3)
        line = 0x4 | 0x1;
    else if (vertex == 12)
        line = 0x8 | 0x1;
    else if (vertex == 7)
        line = 0x1 | 0x2;
    else if (vertex == 8)
        line = 0x2 | 0x8;
}

bool State::operator==(const State &rhs) const {
    return vertex == rhs.vertex;
}

bool State::operator<(const State &rhs) {
    return cost < rhs.cost;
}

bool State::sameline(const State &state) {
    if (state.line & line)
        return true;

    return false;
}

void State::print() const {
    cout << vertex << endl;
}
