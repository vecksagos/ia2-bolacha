#ifndef STATE_H
#define STATE_H

enum class Line : unsigned char { GREEN = 0x1, YELLOW = 0x2, BLUE = 0x4
        , RED = 0x8 };

class State {
public:
    int vertex;
    int cost;
    unsigned char line;

    State(int vertex, int cost = 0);

    bool operator==(const State &rhs) const;
    bool operator<(const State &rhs);

    bool sameline(const State &state);
    void print() const;
};

#endif /* STATE_H */
