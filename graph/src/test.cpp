#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "graph/graph.h"

using namespace std;

SCENARIO("Vertex's graph should be ready", "[graph]") {

    GIVEN("A empty graph") {
        Graph graph;

        WHEN("Add the \"a\" vertex") {
            REQUIRE_NOTHROW(graph.add("a"));

            THEN("Graph's size must be one") {
                REQUIRE(graph.size() == 1);
            }

            THEN("Add another \"a\", a exception must be raised") {
                CHECK_THROWS(graph.add("a"));
                AND_THEN("The graph's size must continue one") {
                    REQUIRE(graph.size() == 1);
                }
            }

            THEN("Add a \"b\" vertex, no exception should be raised") {
                REQUIRE_NOTHROW(graph.add("b"));
                AND_THEN("Graph's size must be two") {
                    REQUIRE(graph.size() == 2);
                }
            }
        }
    }
}

SCENARIO("Edge's graph should be ready", "[graph]") {

    GIVEN("A graph with two vertexes, \"a\" and \"b\"") {
        Graph graph;
        graph.add("a");
        graph.add("b");

        WHEN("Trying to add a edge only origin's vertex exists") {
            THEN("A runtime exception must be throw") {
                REQUIRE_THROWS(graph.add("a", "c", 4));
                AND_THEN("Quantity of edge must be zero") {
                    REQUIRE(graph.connection() == 0);
                }
            }
        }

        WHEN("Trying to add a edge only destination's vertex exists") {
            THEN("A runtime exception must be throw") {
                REQUIRE_THROWS(graph.add("c", "a", 4));
                AND_THEN("Quantity of edge must be zero") {
                    REQUIRE(graph.connection() == 0);
                }
            }
        }

        WHEN("Trying to add a edge only none of vertexes exist") {
            THEN("A runtime exception must be throw") {
                REQUIRE_THROWS(graph.add("c", "c", 4));
                AND_THEN("Quantity of edge must be zero") {
                    REQUIRE(graph.connection() == 0);
                }
            }
        }

        WHEN("Trying to add a edge and both vertexes exist") {
            THEN("A runtime exception must not be throw") {
                REQUIRE_NOTHROW(graph.add("a", "b", 4));
                AND_THEN("Quantity of edge must be one") {
                    REQUIRE(graph.connection() == 1);
                }
            }
        }

        WHEN("Trying to add a edge and both vertexes are the same") {
            THEN("A runtime exception must not be throw") {
                REQUIRE_NOTHROW(graph.add("a", "a", 4));
                AND_THEN("Quantity of edge must be one") {
                    REQUIRE(graph.connection() == 1);
                }
            }
        }
    }
}

SCENARIO("Adjacents should be ok", "[graph]") {

    GIVEN("A graph with 2 vertex: \"a\", \"b\" and \"c\", where a->b, b->c and a->c") {
        Graph graph;
        graph.add("a");
        graph.add("b");
        graph.add("c");

        graph.add("a", "b", 4);
        graph.add("a", "c", 4);
        graph.add("b", "c", 4);

        WHEN("Getting the edges of all vertex") {
            THEN("The vertex \"a\" must have 2 edges") {
                REQUIRE(graph.adjacents("a").size() == 2);
            } AND_THEN("The vertex \"b\" must have 1 edge") {
                REQUIRE(graph.adjacents("b").size() == 1);
            } AND_THEN("The vertex \"c\" must have none") {
                REQUIRE(graph.adjacents("c").size() == 0);
            } AND_THEN("If try get the edge of a no existent vertex, a exception should be raised") {
                REQUIRE_THROWS(graph.adjacents("d"));
            }
        }
    }
}
