#ifndef GRAPH_H
#define GRAPH_H

#include <string>
#include <vector>

class Graph {
public:

    class Edge;
    class Vertex {
    public:
        Vertex(std::string name);

        void print();
        bool is(std::string name);
        std::vector<Edge> adjacents(std::vector<Edge> edges);
        bool operator==(const Vertex &rhs);
        bool operator==(const std::string &rhs);
    private:
        std::string name;
        std::string getName() const;
    };

    class Edge {
    public:
        Edge(Vertex &origin, Vertex &destination, int cost);

        void print();
        bool isOrigin(Vertex &vertex);
    private:
        Vertex &origin;
        Vertex &destination;
        int cost;
    };
    Graph();

    void add(std::string name);
    void add(std::string origin, std::string destination, int cost);
    void print();
    int size();
    int connection();
    std::vector<Edge> adjacents(std::string current);

private:


    std::vector<Vertex> vertexes;
    std::vector<Edge> edges;

};


#endif /* GRAPH_H */
