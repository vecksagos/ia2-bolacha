#ifndef DISJOINTSET_H
#define DISJOINTSET_H

#include <string>
#include <unordered_map>
#include <vector>

class DisjointSet {
private:
    std::unordered_map<Graph::Vertex, Graph::Vertex> parents;
    std::unordered_map<Graph::Vertex, int> rank;

public:
    DisjointSet(std::vector<Graph::Vertex> vertexes);

    std::string find(std::string vertex);
    void join(std::string vertex1, std::string vertex2);
};


#endif /* DISJOINTSET_H */
