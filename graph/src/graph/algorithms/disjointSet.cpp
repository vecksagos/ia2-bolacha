#include "disjointSet.h"

using namespace std;

DisjointSet::DisjointSet(vector<Graph::Vertex> vertexes) {
    for (Graph::Vertex vertex : vertexes) {
        parents[vertex] = vertex;
        rank[vertex] = 0;
    }
}

string find(string vertex) {
    Graph::Vertex aux(vertex);

    while(parents[aux] != aux) {
        aux = parents[aux];
    }

    return aux;
}

void join(string vertex1, string vertex2) {
    Graph::Vertex v1(vertex1), v2(vertex2);

    if (rank[v1] > rank[v2])
        parents[v2] = v1;
    else if (rank[v1] < rank[v2])
        parents[v1] = v2;
    else {
        parents[v1] = v2;
        ++rank[v2];
    }
}
