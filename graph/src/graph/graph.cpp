#include <iostream>
#include <stdexcept>
#include "graph.h"
using namespace std;

// GRAPH
Graph::Graph() {

}

void Graph::print() {
    cout << "Vertexes: " << endl;
    for (auto vertex : vertexes) {
        vertex.print();
        cout << endl;
    }

    cout << "Edges: " << endl;
    for (auto edge : edges)
        edge.print();
}

void Graph::add(string name) {
    for (auto vertex : vertexes) {
        if (vertex == name)
            throw runtime_error("Vertex already exist");
    }

    Vertex vertex(name);
    vertexes.push_back(vertex);
}

void Graph::add(string origin, string destination, int cost) {
    Vertex *o = NULL, *d = NULL;

    for (auto &vertex : vertexes) {
        if (vertex == origin)
            o = &vertex;
        if (vertex == destination)
            d = &vertex;
    }

    if (!o)
        throw runtime_error("Origin vertex does not exist");
    if (!d)
        throw runtime_error("Destination vertex does not exist");

    Edge edge(*o, *d, cost);
    edges.push_back(edge);
}

int Graph::size() {
    return vertexes.size();
}

int Graph::connection() {
    return edges.size();
}

vector<Graph::Edge> Graph::adjacents(string current) {
    Vertex *vertex = NULL;

    for (Vertex v : vertexes)
        if (v == current) {
            vertex = &v;
            break;
        }

    if (!vertex)
        throw runtime_error("Current vertex do not exist!");

    return vertex->adjacents(edges);
}

// VERTEX
Graph::Vertex::Vertex(string name) {
    this->name = name;
}

void Graph::Vertex::print() {
    cout << name;
}

bool Graph::Vertex::is(string name) {
    return this->name == name;
}

vector<Graph::Edge> Graph::Vertex::adjacents(vector<Graph::Edge> edges) {
    vector<Graph::Edge> adjacents;

    for (Edge edge : edges) {
        if (edge.isOrigin(*this))
            adjacents.push_back(edge);
    }

    return adjacents;
}

bool Graph::Vertex::operator==(const Graph::Vertex &rhs) {
    return (this->name == rhs.getName());
}

bool Graph::Vertex::operator==(const std::string &rhs) {
    return (this->name == rhs);
}

string Graph::Vertex::getName() const {
    return name;
}

// EDGE

Graph::Edge::Edge(Vertex &origin, Vertex &destination, int cost)
    : origin(origin)
    , destination(destination)
    , cost(cost) {
}

void Graph::Edge::print() {
    origin.print();
    cout << "->";
    destination.print();
    cout << endl;
}

bool Graph::Edge::isOrigin(Vertex &vertex) {
    return origin == vertex;
}

