#include <iostream>
#include <stdexcept>
#include "graph/graph.h"

using namespace std;

int main(int argc, char **argv) {
    Graph graph;
    graph.add("a");
    graph.add("b");
    try {
        graph.add("b", "a", 4);
    } catch(const runtime_error &e) {
        cout << e.what() << endl;
        return -1;
    }
    graph.print();

    return 0;
}
