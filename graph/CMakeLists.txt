project ( graph )

set( LIB_NAME ${PROJECT_NAME} )
set(LIBRARY_TYPE STATIC)

set( SOURCE ${SOURCE}
  "${PROJECT_SOURCE_DIR}/src/graph/graph.cpp"
  )

include_directories( src )

add_library ( ${LIB_NAME}
  ${LIBRARY_TYPE}
  ${SOURCE}
  )

add_executable( "${PROJECT_NAME}code" src/main.cpp )
add_executable( "${PROJECT_NAME}test" src/test.cpp )


target_link_libraries( "${PROJECT_NAME}code" ${LIB_NAME} )
target_link_libraries( "${PROJECT_NAME}test" ${LIB_NAME} )

enable_testing()
add_test(NAME RunTests COMMAND "${PROJECT_NAME}test")