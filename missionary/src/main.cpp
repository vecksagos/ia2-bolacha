#include <boost/lexical_cast.hpp>
#include "state/state.h"
#include "node/node.h"
#include <utility>
#include <iostream>
#include <vector>
#include <unordered_set>

using boost::lexical_cast;
using boost::bad_lexical_cast;
using namespace std;

enum returns { ERROR, OK };

int main(int argc, char **argv) {
    if ((argc != 5))
        return ERROR;

    pair<int, int> canibais, missionaries;
    try {
        canibais.first = lexical_cast<int>(argv[1]);
        canibais.second = lexical_cast<int>(argv[3]);
        missionaries.first = lexical_cast<int>(argv[2]);
        missionaries.second = lexical_cast<int>(argv[4]);
    } catch(bad_lexical_cast &e) {
        cout << e.what() << endl;
        return ERROR;
    }

    State initial(canibais.first, missionaries.first, Side::LEFT);
    State final(canibais.second, missionaries.second, Side::RIGHT);

    if ((!initial.validator()) || (!final.validator())) {
        cout << "Some state is invalid!" << endl;
        return ERROR;
    }

    Node node(initial, NULL);

    unordered_set<Node> visited;
    vector<Node> frontier;
    frontier.push_back(node);
    auto current = frontier.back();
    while (!frontier.empty()) {
        frontier.pop_back();
        visited.insert(current);
        State state = current.state;
        if (state == final) {
            cout << "Answer:" << endl;
            current.print();
            break;
        }

        auto states = state.generator();
        for (auto son : states) {

            auto father = &*visited.find(current);
            Node gerated(son, father);

            if (visited.find(gerated) == visited.end()) {
                bool repeat = false;
                for (auto no : frontier) {
                    if (no == gerated) {
                        repeat = true;
                        break;
                    }
                }
                if (!repeat) {
                    frontier.push_back(gerated);
                }
            }
        }

        current = frontier.back();
    }

    return OK;
}
