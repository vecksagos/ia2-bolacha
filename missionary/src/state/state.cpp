#include "state.h"
#include <iostream>
#include <stdexcept>

using namespace std;

State::State(int canibais, int missionaries, Side ship)
    : ship(ship) {
    if ((canibais > MAX) || (missionaries > MAX))
        throw runtime_error("Above MAX Error");

    if (ship == Side::LEFT) {
        canibal = make_pair(canibais, MAX - canibais);
        missionary = make_pair(missionaries, MAX - missionaries);
    } else {
        canibal = make_pair(MAX - canibais, canibais);
        missionary = make_pair(MAX - missionaries, missionaries);
    }
}

bool State::operator==(const State& rhs) const {
    return ((ship == rhs.ship) && (canibal == rhs.canibal)
            && (missionary == rhs.missionary));
}

bool State::operator!=(const State& rhs) const {
    return !operator==(rhs);
}

bool State::validator() {
    if (ship == Side::LEFT) {
        if ((canibal.first == 0) && (missionary.first == 0))
            return false;
    } else {
        if ((canibal.second == 0) && (missionary.second == 0))
            return false;
    }

    if ((missionary.first == 0) || (missionary.second == 0))
        return true;

    return ((canibal.first <= missionary.first)
            && (canibal.second <= missionary.second));
}

vector<State> State::generator() {
    vector<State> states;
    if (ship == Side::LEFT) {
        if (canibal.first > 0) {
            {
                State state(canibal.second + 1, missionary.second, Side::RIGHT);
                if (state.validator())
                    states.push_back(state);
            }

            if (canibal.first > 1) {
                State state(canibal.second + 2, missionary.second, Side::RIGHT);
                if (state.validator())
                    states.push_back(state);
            }

            if (missionary.first > 0) {
                State state(canibal.second + 1, missionary.second + 1
                            , Side::RIGHT);
                if (state.validator())
                    states.push_back(state);
            }
        }

        if (missionary.first > 0) {
            {
                State state(canibal.second, missionary.second + 1, Side::RIGHT);
                if (state.validator())
                    states.push_back(state);
            }

            if (missionary.first > 1) {
                State state(canibal.second, missionary.second + 2, Side::RIGHT);
                if (state.validator())
                    states.push_back(state);
            }
        }
    } else {
        if (canibal.second > 0) {
            {
                State state(canibal.first + 1, missionary.first, Side::LEFT);
                if (state.validator())
                    states.push_back(state);
            }

            if (canibal.second > 1) {
                State state(canibal.first + 2, missionary.first, Side::LEFT);
                if (state.validator())
                    states.push_back(state);
            }

            if (missionary.second > 0) {
                State state(canibal.first + 1, missionary.first + 1
                            , Side::LEFT);
                if (state.validator())
                    states.push_back(state);
            }
        }

        if (missionary.second > 0) {
            {
                State state(canibal.first, missionary.first + 1, Side::LEFT);
                if (state.validator())
                    states.push_back(state);
            }

            if (missionary.second > 1) {
                State state(canibal.first, missionary.first + 2, Side::LEFT);
                if (state.validator())
                    states.push_back(state);
            }
        }
    }

    return states;
}

void State::print() const {
    cout << "C:" << canibal.first << " M:" << missionary.first;
    if (ship == Side::LEFT)
        cout << " B...... ";
    else
        cout << " ......B ";

    cout << "C:" << canibal.second << " M:" << missionary.second << endl;
}
