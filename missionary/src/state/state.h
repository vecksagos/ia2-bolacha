#ifndef STATE_H
#define STATE_H

#define MAX 3

#include <utility>
#include <vector>

enum class Side { LEFT, RIGHT };

class State {
public:
    std::pair<int, int> canibal, missionary;
    Side ship;

    bool operator==(const State &rhs) const;
    bool operator!=(const State &rhs) const;
    State(int canibais, int missionaries, Side side);

    bool validator();
    std::vector<State> generator();
    void print() const;
};

#endif /* STATE_H */
