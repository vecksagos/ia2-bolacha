#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "state/state.h"
#include "node/node.h"

using namespace std;

TEST_CASE("State tests for initialization class", "[state]") {
    REQUIRE_THROWS(State state(5, 2, Side::LEFT));
    REQUIRE_NOTHROW(State state(3, 3, Side::RIGHT));
    REQUIRE_NOTHROW(State state(0, 0, Side::RIGHT));
    REQUIRE_NOTHROW(State state(3, 3, Side::LEFT));
    REQUIRE_NOTHROW(State state(0, 0, Side::LEFT));
}

TEST_CASE("State tests for operation ==", "[state]") {
    State state(3, 3, Side::LEFT);
    State oState(3, 3, Side::LEFT);
    REQUIRE(state == oState);

    state.ship = Side::RIGHT;
    oState.ship = Side::RIGHT;
    REQUIRE(state == oState);

    state.canibal = make_pair(0, 3);
    state.missionary = make_pair(0, 3);
    oState.canibal = make_pair(0, 3);
    oState.missionary = make_pair(0, 3);
    REQUIRE(state == oState);

    state.ship = Side::LEFT;
    oState.ship = Side::LEFT;
    REQUIRE(state == oState);

    state.ship = Side::RIGHT;
    REQUIRE(state != oState);
}

TEST_CASE("State tests for validator method", "[state]") {
    {
        State state(3, 2, Side::LEFT);
        REQUIRE_FALSE(state.validator());

        state.canibal = make_pair(2, 1);
        REQUIRE(state.validator());
    }

    {
        State state(3, 3, Side::RIGHT);
        REQUIRE(state.validator());

        state.missionary = make_pair(3, 0);
        REQUIRE(state.validator());
    }

    {
        State state(3, 3, Side::LEFT);
        state.ship = Side::RIGHT;

        REQUIRE_FALSE(state.validator());
    }
}

TEST_CASE("State tests for generator method", "[state]") {
    {
        State state(3, 3, Side::LEFT);

        REQUIRE(state.generator().size() == 3);
    }

    State state(3, 0, Side::RIGHT);
    State state2(1, 3, Side::LEFT);

    REQUIRE(state.generator().size() == 2);
    REQUIRE(state2.generator().size() == 2);
}

TEST_CASE("Node tests for isSolution method", "[node]") {
    State state(0, 0, Side::RIGHT);
    State solution(0, 0, Side::RIGHT);
    State notSolution(3, 3, Side::LEFT);
    State notOtherSolution(0, 0, Side::LEFT);

    Node node(state, NULL);
    REQUIRE(node.isSolution(solution));
    REQUIRE_FALSE(node.isSolution(notOtherSolution));
    REQUIRE_FALSE(node.isSolution(notSolution));
}

TEST_CASE("Node tests for father", "[node]") {
    State father(1, 1, Side::RIGHT);
    State son(3, 3, Side::LEFT);
    State grandfather(3, 3, Side::LEFT);

    Node grandfathernode(grandfather, NULL);
    Node fathernode(father, &grandfathernode);
    Node sonnode(son, &fathernode);

    REQUIRE(father == sonnode.father->state);
    REQUIRE(grandfather == fathernode.father->state);
    REQUIRE(son == fathernode.father->state);
}
