#include "node.h"
#include <iostream>

using namespace std;

Node::Node(State state, const Node *father)
    : father(father)
    , state(state) {

}

bool Node::isSolution(State solution) {
    return (state == solution);
}

bool Node::operator==(const Node &rhs) const {
    return state == rhs.state;
}

void Node::print() const {
    if (father != NULL)
        father->print();

    state.print();
}
